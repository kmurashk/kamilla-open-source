Kamilla's Open Source Projects
------------------------------

Here my open-source C++, Java, Python 
lectures and programs that I created during my teaching in previous years.

- [C++ for Beginners Lectures](https://gitlab.com/kmurashk/cpp-for-beginners-lectures)

- [C++ for Beginners](https://gitlab.com/kmurashk/cpp-for-beginners)

- [C++ Data Structures](https://gitlab.com/kmurashk/cpp-data-structures)

- [C++ for Intermediate](https://gitlab.com/kmurashk/cpp-for-intermediate)

- [Java for Beginners](https://gitlab.com/kmurashk/java-for-beginners)

- [Java Data Structures](https://gitlab.com/kmurashk/java-data-sructures)

- [Python for Beginners Handouts](https://gitlab.com/kmurashk/python-for-beginners)

- [Objects and Algorithms Handouts and Assignments](https://gitlab.com/kmurashk/objects-algorithms-assignments)

- [Python for Business](https://gitlab.com/kmurashk/python-for-bus)